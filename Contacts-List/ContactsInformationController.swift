//
//  ContactsInformationController.swift
//  Contacts-List
//
//  Created by Bohdan Hordiienko on 5/15/17.
//  Copyright © 2017 HorBog. All rights reserved.
//

import UIKit

class ContactsListViewController: UIViewController {
 
    
    @IBOutlet weak var lbFirstName: UILabel!
    @IBOutlet weak var lbSecondName: UILabel!
    @IBOutlet weak var lbPhoneNumber: UILabel!
    @IBOutlet weak var lbEMail: UILabel!
    
}
