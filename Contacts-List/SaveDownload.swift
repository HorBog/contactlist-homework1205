//
//  SaveDownload.swift
//  Contacts-List
//
//  Created by Bohdan Hordiienko on 5/15/17.
//  Copyright © 2017 HorBog. All rights reserved.
//

import Foundation

class SaveDownload {
    
    var gotArray = String()
    
    let file = "file.txt" //this is the file. we will write to and read from it
    
    func saveData(array: String) {
        
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let path = dir.appendingPathComponent(file)
            
            //writing
            do {
                try array.write(to: path, atomically: false, encoding: String.Encoding.utf8)
                
            }
            catch {/* error handling here */}
            
        }
    }
    
    func readData () -> [String] {
        
        var arrayToReturn = [String]()
        var encriptedText = ""
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
        
            let path = dir.appendingPathComponent(file)
            
            //reading
            do {
                encriptedText = try String(contentsOf: path, encoding: String.Encoding.utf8)
    
            }
            catch {/* error handling here */}
        }
        
        arrayToReturn = encriptedText.components(separatedBy: "*")
        arrayToReturn.remove(at: arrayToReturn.count - 1)
        
      
        
        return arrayToReturn
    }
}
