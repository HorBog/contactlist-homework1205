//
//  User.swift
//  Contacts-List
//
//  Created by Богдан on 13.05.17.
//  Copyright © 2017 HorBog. All rights reserved.
//

import Foundation

class User {
    
    private var name = ""
    private var surname = ""
    private var phoneNumber = ""
    private var eMail = ""
    
    
    
    init(name: String, surname: String, phoneNumber: String, eMail:String) {
        
        self.name = name
        self.surname = surname
        self.phoneNumber = phoneNumber
        self.eMail = eMail
    }
    
    func setName (name: String) {
        
        self.name = name
    }
    
    func getName() -> String {
        
        return self.name
    }
    
    func setSurname (surname: String) {
        
        self.surname = surname
    }
    
    func getSurname() -> String {
        
        return self.surname
    }
    
    func setPhoneNumber (phoneNumber: String) {
        
        self.phoneNumber = phoneNumber
    }
    
    func getPhoneNumber() -> String {
        
        return self.phoneNumber
    }
    
    func setEMail (eMail: String) {
        
        self.eMail = eMail
    }
    
    func getEMail() -> String {
        
        return self.eMail
    }
    
    
}
