//
//  ViewController.swift
//  Contacts-List
//
//  Created by Богдан on 13.05.17.
//  Copyright © 2017 HorBog. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var tfEMail: UITextField!
    
    var base : TableViewController?
    var cont_original : User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let contact = cont_original {
            
            tfFirstName.text = contact.getName()
            tfLastName.text = contact.getSurname()
            tfPhoneNumber.text = contact.getPhoneNumber()
            tfEMail.text = contact.getEMail()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func fnSave(_ sender: Any) {
        
        guard let firstName = tfFirstName.text else {
            tfFirstName.becomeFirstResponder()
            return
        }
        guard let lastName = tfLastName.text else {
            tfLastName.becomeFirstResponder()
            return
        }
        guard let phoneNumber = tfPhoneNumber.text else {
            tfPhoneNumber.becomeFirstResponder()
            return
        }
        guard let eMail = tfEMail.text else {
            tfEMail.becomeFirstResponder()
            return
        }
        
        
        let contact = User(name: firstName, surname: lastName, phoneNumber: phoneNumber, eMail: eMail)
        
        if cont_original == nil {
            
            let contact = User(name: firstName, surname: lastName, phoneNumber: phoneNumber, eMail: eMail)
            base?.addContact(contact: contact)
        } else {
            
            cont_original?.setName(name: firstName)
            cont_original?.setSurname(surname: lastName)
            cont_original?.setPhoneNumber(phoneNumber: phoneNumber)
            cont_original?.setEMail(eMail: eMail)
            
            base?.tableView.reloadData()
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func fnCencel(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
}

