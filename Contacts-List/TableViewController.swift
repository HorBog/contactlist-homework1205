//
//  TableViewController.swift
//  Contacts-List
//
//  Created by Богдан on 13.05.17.
//  Copyright © 2017 HorBog. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    let controlerInformationAboutContact = "InformationAboutContact"
    
    var contactListInString = String()
    var contactList: [User] = []
    var downloadedContactList = [User]()
    var savedData = [String]()
    
    let saveDownload = SaveDownload()
    
    func addContact (contact : User) {
        
        contactList.append(contact)
        print("Contacts count: \(contactList.count)")
        self.manageContactsToStringArray(contactList: contactList)
        saveDownload.saveData(array: contactListInString)
        tableView.reloadData()
    }
    
    // Converting contasts from object User to string
    var i = 0
    func manageContactsToStringArray(contactList: [User]){
        
        while i < contactList.count {
            
            contactListInString.append(contactList[i].getName() + ("*"))
            contactListInString.append(contactList[i].getSurname() + ("*"))
            contactListInString.append(contactList[i].getPhoneNumber() + ("*"))
            contactListInString.append(contactList[i].getEMail() + ("*"))
            
            i += 1
        }
    }
    
    func getContactStringInArray() -> String {
        
        return contactListInString
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = UserDefaults.standard
        contactList = interprateDownloadArrayToUser(contactsArray: saveDownload.readData())
       // let savedArray = NSArray(contentsOfURL : "/tmp/foo.plist") as! [[String]]
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    func interprateDownloadArrayToUser(contactsArray: [String]) -> [User]{
        var i = 0
        if contactsArray.count > 0 {
        while i < contactsArray.count {
            let user = User(name: contactsArray[i], surname: contactsArray[i + 1], phoneNumber: contactsArray[i + 2], eMail: contactsArray[i + 3])
            downloadedContactList.append(user)
            
                i += 4
            
            }
        } else {
            
            downloadedContactList = [User]()
        }
        
        return downloadedContactList
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return contactList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let line = indexPath.row
        let cell = tableView.dequeueReusableCell(withIdentifier: (line % 2 == 0 ? "contactLine" : "contactLine2"), for: indexPath)
        // Configure the cell...
        
        let name = contactList[line].getName() + " " + contactList[line].getSurname()
        
        cell.textLabel?.text = name
        cell.detailTextLabel?.text = contactList[line].getPhoneNumber()
        cell.accessoryType = .disclosureIndicator
    
        return cell
    }
    

    @IBAction func edidContact(_ sender: Any) {
        
        tableView.setEditing(!tableView.isEditing, animated: true)
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            contactList.remove(at: indexPath.row)
            
            if contactList.count < 1 {
                
                saveDownload.saveData(array: "*")
            } else {
            
                saveDownload.saveData(array: contactListInString)
            }
            
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.reloadData()
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
 

    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let line1 = fromIndexPath.row
        let line2 = to.row
        
        let temp = contactList[line1]
        contactList[line1] = contactList[line2]
        contactList[line2] = temp
    }
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
       // performSegue(withIdentifier: <#T##String#>, sender: <#T##Any?#>)
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "Contact") as! ViewController
        vc.base = self
        vc.cont_original = contactList[indexPath.row]
        
        navigationController?.show(vc, sender: self)
    }

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "contactAdding"{
            
            let viewController = segue.destination as! ViewController
            viewController.base = self
        }
    }
}
